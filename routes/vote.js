const express = require("express");

const router = express.Router();

module.exports = (params) => {
  const { loginService } = params;

  router.get("/", async (request, response, next) => {
    response.send("Next question, please.");
  });

  router.post("/", async (request, response) => {
    const { optradio, user, survey, question } = request.body;
    console.log(
      "/vote post choice: ",
      optradio,
      "user: ",
      user,
      "survey: ",
      survey,
      "question: ",
      question
    );
    // try {
    //   // exists?
    //   const rs = await loginService.dbgetAuthor(email, password);
    //   // also, better call Saul...

    //   if (rs.length > 0) {
    //     console.log("login.js query returned something");
    //     request.session.feedback = { message: "Logged In" };
    //     request.session.user = email;
    //   } else {
    //     request.session.loggedin = false;
    //     request.session.user = "";

    //     request.session.feedback = {
    //       message: "User / password mismatch",
    //     };
    //   }
    // } catch (err) {
    //   console.log(`login.js error ${err}`);
    // }
    return response.redirect("/response");
  });

  return router;
};