const express = require('express');

const router = express.Router();

module.exports = params => {

    const { questionService } = params;

    router.get("/", (request, response) => {
        const user = request.session.user ? request.session.user : '';
        response.render("layout", { pageTitle: "Questions", template: "question", user: user });
    });

    return router;
};