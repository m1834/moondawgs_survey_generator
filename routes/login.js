const express = require('express');

const router = express.Router();

module.exports = params => {
  const { loginService } = params;

  router.get('/', async (request, response, next) => {
    try {
      const errors = request.session.feedback ? request.session.feedback.errors : false;
      const successMessage = request.session.feedback ? request.session.feedback.message : false;
      const user = request.session.user ? request.session.user : '';
      const loggedin = request.session.loggedin ? request.session.loggedin : false;
      
      request.session.feedback = {};

      console.log('routes/login get / seems happy');
      return response.render('layout', {
        pageTitle: 'Login',
        template: 'login',
        user: user,
        loggedin: loggedin,
        successMessage,
      });
    } catch (err) {
      return next(err);
    }
  });

  router.post('/', async (request, response) => {
    const { email, password } = request.body;
    console.log('post /login: ', email, ' password: ', password);
    try {
      // exists?
      const rs = await loginService.dbgetAuthor(email, password);
      // also, better call Saul...

      if (rs.length > 0) {
        console.log('login.js query returned something');
        request.session.feedback = { message: 'Logged In' };
        request.session.user = email;
        request.session.loggedin = true;
        return response.redirect('/survey');
      } else {
        request.session.loggedin = false;
        request.session.user = '';

        request.session.feedback = {
          message: 'User / password mismatch',
        };
      }
    } catch (err) {
      console.log(`login.js error ${err}`);
    }
    return response.redirect('/login');
  });
  return router;
};