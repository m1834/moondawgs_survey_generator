const express = require('express');

const router = express.Router();
//const script = require('././js/script');

module.exports = params => {
    const { generateService } = params;
    
    router.get("/", (request, response) => {
        response.render("layout", { pageTitle: "Generate", template: "generate" });
    });

    router.post('/', (request, response) => {
        return response.send('Generate form posted');
    });

    return router;
};