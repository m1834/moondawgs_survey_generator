const express = require('express');

const router = express.Router();

module.exports = params => {
    const { surveyService } = params;

    router.get("/:id/u/:user", async (request, response) => {
      let parms = request.params;
      console.log(`survey.js rout 1/2/3 ${parms}`);
  
      let survey = request.params.id;
      //let question = request.params.question;
      let user = request.params.user;

      const dbquestions = await surveyService.dbgetQuestionData(survey, user);

      // return response.send(
      //   `On survey ${survey} ask ${user} question ${question}`
      // );
      // get fields to populate the ejs form
      return response.render("layout", {
        pageTitle: "Deploy",
        template: "deploy",
        user: user,
        //question: question,
        survey: survey,
        dbquestions: dbquestions,
      });
    });
    
    router.get("/", async (request, response) => {
        const surveys = await surveyService.dbgetData();
    
        try {
          const errors = request.session.feedback
            ? request.session.feedback.errors
            : false;
          const successMessage = request.session.feedback
            ? request.session.feedback.message
            : false;
          const user = request.session.user ? request.session.user : "";

          request.session.feedback = {};
    
          console.log("routes/survey get / seems happy");
          return response.render("layout", {
            pageTitle: "Survey",
            template: "survey",
            user: user,
            successMessage,
          });
        } catch (err) {
          return next(err);
        }
    });

    router.post("/", async (request, response) => {
        const { survey_title, author_id, date, purpose, prompt1, type1, prompt2, type2} = request.body;
        console.log("post /survey: ", survey_title, " by: ", author_id, " on: ", date, " for: " , purpose);
        try {
          // exists?
          // const rs = await surveyService.dbCreateSurvey(title, begindate);
          const rs = await surveyService.createSurvey(survey_title, author_id, date, purpose, prompt1, type1, prompt2, type2);
          console.log("survey.js post/ after createSurvey", rs);
          if (rs) {
            console.log("survey.js query returned something:", rs);
            request.session.feedback = { message: `Inserted Survey ID: ${rs}` };
            request.session.title = "";
            request.session.begindate = "";
            // request.session.rs = rs;
            // request.session.user = email;
          } else {
            request.session.title = "";
            request.session.begindate = "";
    
            request.session.feedback = {
              message: "Survey not inserted",
            };
          }
        } catch (err) {
          console.log(`survey.js error ${err}`);
        }
        return response.redirect("/survey");
      });

    return router;
};