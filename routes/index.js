const express = require("express");

const surveyRoute = require("./survey");
const generateRoute = require("./generate");
const responseRoute = require("./response");
const loginRoute = require("./login");
const questionRoute = require("./question");
const voteRoute = require("./vote");

const router = express.Router();

module.exports = (params) => {
    router.use("/generate", generateRoute(params));

  // router.get('/survey', (request, response) => {
  // response.render('layout', { pageTitle: 'Surveys', template: 'survey' });
  // });

  router.use("/vote", voteRoute(params));
  
  router.use("/survey/:id/:user", surveyRoute(params));
  router.use("/survey", surveyRoute(params));

  router.use("/response", responseRoute(params));

  router.use("/question", questionRoute(params));
  
  router.use("/login", loginRoute(params));

  router.get("/logout", (request, response) => {
    request.session.user = '';
    request.session.loggedin = false;
    return response.redirect('/login');
  });

  router.get("/", (request, response) => {
    request.session.user = '';
    request.session.loggedin = false;
    response.render("layout", { pageTitle: "Welcome", template: "index" });
  });

  return router;
};