const express = require('express');

const router = express.Router();

module.exports = params => {

    const { responseService } = params;

    router.get("/", (request, response) => {
      const user = request.session.user ? request.session.user : '';  
      response.render("layout", { pageTitle: "Responses", template: "response", user: user });
      });

    return router;
};