# moondawgs_survey_generator

The objective of this project is to create a survey generator that allows a user (survey author) to make surveys, send them to respondents, and view statistical data of the responses.