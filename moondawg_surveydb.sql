-- moondawg_survey_db.sql
-- IT325 Reeves
-- 
--Moondawgs Survey Generator Database
--11/08/21
--
--Group: moondawgs
--
--Run in psql with: \i moondawg_surveydb.sql

--CREATE DATABASE
drop database if exists moondawg_survey;
CREATE DATABASE moondawg_survey encoding 'UTF-8';
\c moondawg_survey;

--CREATE AUTHOR TABLE
DROP TABLE if exists author;
CREATE TABLE author (
author_id int primary key NOT NULL,
email text,
password text);

--INSERT AUTHORS
--In future replace plain text passwords with encryption
INSERT into author (author_id, email, password) VALUES (1, 'brent.reeves@blee.com', 'Purple789!');
INSERT into author (author_id, email, password) VALUES (2, 'phil.schubs@acu.org', 'Acuiscool123!');
INSERT into author (author_id, email, password) VALUES (3, 'justin.raitz@sports.com', 'Ilovesports14!');

--CREATE SURVEY TABLE
drop table if exists survey;
CREATE TABLE survey (
survey_id SERIAL UNIQUE,
title text,
purpose text,
author int,
beginDate date);

--INSERT SURVEYS
INSERT into survey (title, purpose, author, beginDate) VALUES ('Should You Be Fired?', 'Find out if person should be fired.', 1, '2021/11/08');
INSERT into survey (title, purpose, author, beginDate) VALUES ('ACU Students', 'See if students like ACU.', 2, '2021/11/09');
INSERT into survey (title, purpose, author, beginDate) VALUES ('The Sports Survey', 'Just cause sports are great.', 3, '2021/11/10');

--CREATE QUESTION TABLE
drop table if exists question;
CREATE TABLE question (
question_id SERIAL UNIQUE,
survey_id int,
prompt text,
question_type char(2));

--INSERT QUESTIONS
INSERT into question (survey_id, prompt, question_type) VALUES (1, 'You should be fired.', 'TF');
INSERT into question (survey_id, prompt, question_type) VALUES (1, 'You should really be fired.', 'TF');
INSERT into question (survey_id, prompt, question_type) VALUES (2, 'ACU is a great college.', 'L4');
INSERT into question (survey_id, prompt, question_type) VALUES (2, 'ACU could improve.', 'L4');
INSERT into question (survey_id, prompt, question_type) VALUES (3, 'Sports are fun.', 'L5');
INSERT into question (survey_id, prompt, question_type) VALUES (3, 'Basketball is the best sport.', 'L5');

--CREATE CHOICE TABLE
DROP TABLE if exists choice;
CREATE TABLE choice (
question_id int,
option_n int,
primary key (question_id, option_n),
choice text
);

--INSERT CHOICES
INSERT into choice (question_id, option_n, choice) VALUES (1, 1, 'True');
INSERT into choice (question_id, option_n, choice) VALUES (1, 2, 'False');

--CREATE SURVEY_QUESTION REALTIONSHIP TABLE
drop table if exists survey_question;
CREATE TABLE survey_question (
survey_id int,
question_id int);

--INSERT RELATIONSHIPS
INSERT into survey_question (survey_id, question_id) VALUES (1, 1);

--CREATE USERS TABLE
drop table if exists users;
CREATE TABLE users (
user_id int primary key);

--INSERT USERS
INSERT into users (user_id) VALUES (1);
INSERT into users (user_id) VALUES (2);

--CREATE USERS_SURVEY RELATIONSHIP TABLE
drop table if exists users_survey;
CREATE TABLE users_survey (
user_id int,
survey_id int);

--INSERT RELATIONSHIPS
INSERT into users_survey (user_id, survey_id) VALUES (1, 1);
INSERT into users_survey (user_id, survey_id) VALUES (1, 2);
INSERT into users_survey (user_id, survey_id) VALUES (1, 3);
INSERT into users_survey (user_id, survey_id) VALUES (2, 1);
INSERT into users_survey (user_id, survey_id) VALUES (2, 2);
INSERT into users_survey (user_id, survey_id) VALUES (2, 3);

--CREATE RESPONSE TABLE
DROP TABLE if exists response;
CREATE TABLE response (
response_id SERIAL UNIQUE,
user_id int,
survey_id int,
question_id int,
answer text);

--INSERT RESPONSES
INSERT into response (user_id, survey_id, question_id, answer) VALUES (1, 1, 1, 'True');
INSERT into response (user_id, survey_id, question_id, answer) VALUES (1, 1, 2, 'True');
INSERT into response (user_id, survey_id, question_id, answer) VALUES (2, 1, 1, 'False');
INSERT into response (user_id, survey_id, question_id, answer) VALUES (2, 1, 2, 'False');
INSERT into response (user_id, survey_id, question_id, answer) VALUES (1, 2, 1, 'Strongly Agree');
INSERT into response (user_id, survey_id, question_id, answer) VALUES (1, 2, 2, 'Disagree');
INSERT into response (user_id, survey_id, question_id, answer) VALUES (2, 2, 1, 'Agree');
INSERT into response (user_id, survey_id, question_id, answer) VALUES (2, 2, 2, 'Strongly Disagree');
INSERT into response (user_id, survey_id, question_id, answer) VALUES (1, 3, 1, 'Neutral');
INSERT into response (user_id, survey_id, question_id, answer) VALUES (1, 3, 2, 'Disagree');
INSERT into response (user_id, survey_id, question_id, answer) VALUES (2, 3, 1, 'Strongly Agree');
INSERT into response (user_id, survey_id, question_id, answer) VALUES (2, 3, 2, 'Agree');


--Create moondawgboss role
CREATE role moondawgboss login password 'Moonies56';

--Grant moondawgboss access to all tables
--CAUTION !! Bad in real world
GRANT all on author to moondawgboss;
GRANT all on survey to moondawgboss;
GRANT all on question to moondawgboss;
GRANT all on question_question_id_seq to moondawgboss;
GRANT all on survey_question to moondawgboss;
GRANT all on users to moondawgboss;
GRANT all on users_survey to moondawgboss;
GRANT all on response to moondawgboss;
GRANT all on response_response_id_seq to moondawgboss;
GRANT all on survey_survey_id_seq to moondawgboss;
