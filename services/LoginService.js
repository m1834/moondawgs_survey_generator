// const util = require('util');
const db = require('./db');

/**
 * Logic for reading and writing feedback data
 */
class LoginService {
  /**
   * Constructor
   */
  constructor() {
    this.db = db;
  }

  /**
   * Get all feedback items
   */
  async getList() {
    const data = await this.getData();
    return data;
  }

  /**
   * Fetches feedback data from the JSON file provided to the constructor
   */

  async dbgetList() {
    const data = await this.dbgetData();
    return data;
  }

  async dbgetAuthor(email, password) {
    // await this.db.dbgetLogins();  => Cannot read property 'dbgetLogins' of undefined
    console.log('loginService dbgetAuthor() called: ', email, password);
    // eslint-disable-next-line no-unused-vars
    const data = await this.db.dbgetAuthor(email, password).catch(err => {
      console.log(`loginService dbgetAuthor err: ${err}`);
      return [];
    });
    return data;
  }
}

module.exports = LoginService;
