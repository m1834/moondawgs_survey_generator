const fs = require('fs');
const util = require('util');

const writeFile = util.promisify(fs.writeFile);

/**
 * Logic for reading and writing new survey data
 */
class GenerateService {
  /**
   * Constructor
   * @param {*} datafile Path to a JSOn file that contains the new survey data
   */
  constructor(datafile) {
    this.datafile = datafile;
  }

  /**
   * Add a new survey item
   * @param {*} title The title of the new survey
   * @param {*} purpose The purpose of the new survey
   * @param {*} author The author of the new survey
   */
  async addEntry(title, purpose, author) {
    const data = (await this.getData()) || [];
    data.unshift({ title, purpose, author });
    return writeFile(this.datafile, JSON.stringify(data));
  }
}

module.exports = GenerateService;
