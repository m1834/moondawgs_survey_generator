/* eslint-disable no-lonely-if */
const dotenv = require("dotenv");
// const express = require('express');
dotenv.config();

// import pkg from 'pg';
const pkg = require("pg");

const { Pool } = pkg;
let pool = "";

// const dot = require('dotenv').config();
const { DATABASE_URL_LOCAL, DATABASE_URL, DB_PASSWORD } = process.env;
console.log("process.env.NODE_ENV", process.env.NODE_ENV);

console.log("DATABASE_URL_LOCAL", DATABASE_URL_LOCAL);
console.log("DATABASE_URL", DATABASE_URL);
console.log("DATABASE_PASSWORD", DB_PASSWORD);

// from docs:
// const db = require('db')
// db.connect({
//   host: process.env.DB_HOST,
//   username: process.env.DB_USER,
//   password: process.env.DB_PASS
// })

if (typeof DATABASE_URL_LOCAL !== "undefined") {
  console.log("DATABASE_URL_LOCAL", DATABASE_URL_LOCAL);
  pool = new Pool({
    connectionString: DATABASE_URL_LOCAL,
  });
  // console.log('Pool:', pool);
} else {
  if (typeof DATABASE_URL !== "undefined") {
    console.log("DATABASE_URL", DATABASE_URL);
    pool = new Pool({
      connectionString: DATABASE_URL,
      ssl: { rejectUnauthorized: false },
    });
  } else {
    console.log("ERROR - must define DATABASE_URL in process variables");
  }
}

console.log("index.js starting...\n");

// export async function connectToDatabase() {
// async function connectToDatabase() {
//   return pool;
// }

async function query(sql, errorMessage) {
  try {
    const client = await pool.connect();
    // console.log('after connect client:', client);
    const result = await client.query(sql);
    const results = result ? result.rows : null;
    client.release();

    return results;
    // return {
    //   status: 200,
    //   body: {
    //     results,
    //   },
    // };
  } catch (err) {
    console.error(`ERROR: db ${errorMessage} sql: ${sql}`, err);

    return {
      status: 500,
      body: {
        error: `ERROR: db ${errorMessage}. ${err}`,
      },
    };
  }
}

async function dbquery(sql, vars = null) {
  try {
    const client = await pool.connect();
    // console.log('after connect client:', client);
    var result = "";
    if (vars == null) {
      result = await client.query(sql);
    } else {
      result = await client.query(sql, vars);
    }
    const results = result ? result.rows : null;
    client.release();

    return results;
    // return {
    //   status: 200,
    //   body: {
    //     results,
    //   },
    // };
  } catch (err) {
    console.error(`ERROR: dbquery ${err}`);

    return {
      status: 500,
      body: {
        error: `ERROR: dbquery ${err}`,
      },
    };
  }
}

async function dbgetQuestions() {
  return this.query(
    "select question_id, survey_id, prompt, question_type from question order by question_id",
    null
  );
}

async function dbgetQuestion(survey, user) {
  console.log("dbgetQuestions survey:", survey, " user: ", user);
  let rs = await this.dbquery(
    "select question_id, survey_id, prompt, question_type from question where survey_id = $1",
    [survey]
  ).catch((err) => {
    console.log(`db.dbgetQuestion troubles ${err}`);
    return err;
  });
  console.log("rs: ", rs);
  return rs;
}

async function dbgetSurveys() {
  return this.query(
    "select survey_id, title, purpose, author, begindate from survey order by survey_id",
    null
  );
}

async function dbgetResponses() {
  return this.query(
    "select response_id, user_id, survey_id, question_id, answer from response order by response_id",
    null
  );
}

async function dbgetAuthor(email, password) {
  console.log("dbgetAuthor email:", email, " password: ", password);
  // sql = 'select id, email, password from author where email = ' + email + ' and password = ' + pwd',
  let rs = await this.dbquery(
    "select author_id, email, password from author where email = $1 and password = $2 limit 1",
    [email, password]
  ).catch((err) => {
    console.log(`db.dbgetAuthor troubles ${err}`);
    return err;
  });
  console.log("rs: ", rs);
  return rs;
}

/**
 * returns: integer or NULL
 */

async function dbInsertSurvey(survey_title, author_id, date, purpose, prompt1, type1, prompt2, type2) {
  let rc = null;
  console.log("dbInsertSurvey called: ", survey_title, author_id, date, purpose, prompt1, type1, prompt2, type2);
  const fields = [survey_title, purpose, author_id, date];
  let rs = await this.dbquery(
    "insert into survey (title, purpose, author, begindate) values ($1, $2, $3, $4)",
    fields
  )
    .then(async (data) => {
      console.log("dbInsertSurvey happy:", data);
      rs = await this.dbquery("select currval('survey_survey_id_seq')")
        .then((rr) => {
          console.log("dbInsert currval: ", rr, rr.rows, rr[0], rr[0].currval);
          rc = rr[0].currval;
        })
        .catch((err) => {
          console.log("dbInsert currval() err: ", err);
        });
    })
    .catch((err) => {
      console.log(`dbInsertSurvey this.dbQuery err: ${err}`);
    });

  console.log("dbInsert insert returning: ", rc);
  
  const fields2 = [rc, prompt1, type1];
  console.log("First element in fields2: ", fields2[0], "Second: ", fields2[1], "Third: ", fields2[2]);
  if(fields2[1] != '' && fields2[2] != '') {
    let rs2 = await this.dbquery(
      "insert into question (survey_id, prompt, question_type) values ($1, $2, $3)",
      fields2
    )
  } 

  const fields3 = [rc, prompt2, type2];
  if(fields3[1] != '' && fields3[2] != '') {
    let rs3 = await this.dbquery(
      "insert into question (survey_id, prompt, question_type) values ($1, $2, $3)",
      fields3
    ) 
  }

  return rc;
}

module.exports = {
  dbgetQuestions,
  dbgetQuestion,
  dbgetAuthor,
  dbquery,
  query,
  dbInsertSurvey,
  dbgetSurveys,
  dbgetResponses,
};
