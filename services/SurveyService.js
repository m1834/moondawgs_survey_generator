const fs = require("fs");
const util = require("util");
const db = require('./db');

/**
 * We want to use async/await with fs.readFile - util.promisfy gives us that
 */
const readFile = util.promisify(fs.readFile);

/**
 * Logic for fetching surveys information
 */
class SurveyService {
  /**
   * Constructor
   * @param {*} datafile Path to a JSOn file that contains the surveys data
   */
  constructor(datafile) {
    this.datafile = datafile;
    this.db = db;
  }

  /**
   * Returns a list of surveys name and short name
   */
  async getTitles() {
    const data = await this.getData();

    // We are using map() to transform the array we get into another one
    return data.map(survey => {
      return { title: survey.title };
    });
  }

  /**
   * Get survey information provided a shortname
   * @param {*} shortname
   */
  /*async getsurvey(shortname) {
    const data = await this.getData();
    const survey = data.find(elm => {
      return elm.shortname === shortname;
    });
    if (!survey) return null;
    return {
      title: survey.title,
      name: survey.name,
      shortname: survey.shortname,
      description: survey.description
    };
  }*/

  /**
   * Get a list of surveys
   */
  async getList() {
    const data = await this.getData();
    return data.map(survey => {
      return {
        id: survey.id,
        title: survey.title,
        purpose: survey.purpose,
        author: survey.author
      };
    });
  }

  /**
   * Fetches surveys data from the JSON file provided to the constructor
   */
  async getData() {
    const data = await readFile(this.datafile, "utf8");
    return JSON.parse(data).surveys;
  }

  async dbgetList() {
    const data = await this.dbgetData();
    return data;
  }

  async dbgetData() {
    // await this.db.dbgetQuestions();  => Cannot read property 'dbgetQuestions' of undefined
    console.log('dbgetData() called');
    // eslint-disable-next-line no-unused-vars
    const data = await this.db.dbgetSurveys().catch(err => {
      return [];
    });
    // const data = await readFile(this.datafile, 'utf8');
    // return JSON.parse(data);
    // console.log('dbgetData ', data);
    return data;
  }

  async dbgetQuestionData(survey, user) {
    // await this.db.dbgetQuestions();  => Cannot read property 'dbgetQuestions' of undefined
    console.log('dbgetQuestionData() called');
    // eslint-disable-next-line no-unused-vars
    const data = await this.db.dbgetQuestion(survey, user).catch(err => {
      return [];
    });
    // const data = await readFile(this.datafile, 'utf8');
    // return JSON.parse(data);
    // console.log('dbgetData ', data);
    return data;
  }

  async createSurvey(survey_title, author_id, date, purpose, prompt1, type1, prompt2, type2) {
    console.log("surveyService createSurvey() called: ", survey_title, author_id, date, purpose, prompt1, type1, prompt2, type2);
    // eslint-disable-next-line no-unused-vars
    const data = await this.db.dbInsertSurvey(survey_title, author_id, date, purpose, prompt1, type1, prompt2, type2).catch((err) => {
      console.log(`surveyService createSurvey db err: ${err}`);
      return [];
    });
    console.log("surveyService createSurvey() received: ", data);
    return data;
  }
}

module.exports = SurveyService;