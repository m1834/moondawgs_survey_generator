const fs = require("fs");
const util = require("util");
const db = require('./db');

/**
 * We want to use async/await with fs.readFile - util.promisfy gives us that
 */
const readFile = util.promisify(fs.readFile);

/**
 * Logic for fetching responses information
 */
class ResponseService {
  /**
   * Constructor
   * @param {*} datafile Path to a JSOn file that contains the responses data
   */
  constructor(datafile) {
    this.datafile = datafile;
    this.db = db;
  }

  /**
   * Returns a list of responses name and short name
   */
  /*async getTitles() {
    const data = await this.getData();

    // We are using map() to transform the array we get into another one
    return data.map(response => {
      return { title: response.title };
    });
  }*/

  /**
   * Get response information provided a shortname
   * @param {*} shortname
   */
  /*async getresponse(shortname) {
    const data = await this.getData();
    const response = data.find(elm => {
      return elm.shortname === shortname;
    });
    if (!response) return null;
    return {
      title: response.title,
      name: response.name,
      shortname: response.shortname,
      description: response.description
    };
  }*/

  /**
   * Get a list of responses
   */
  async getList() {
    const data = await this.getData();
    return data.map(response => {
      return {
        id: response.id,
        user_id: response.user_id,
        survey_id: response.survey_id,
        question_id: response.question_id,
        answer: response.answer
      };
    });
  }

  /**
   * Fetches responses data from the JSON file provided to the constructor
   */
  async getData() {
    const data = await readFile(this.datafile, "utf8");
    return JSON.parse(data).responses;
  }

  async dbgetList() {
    const data = await this.dbgetData();
    return data;
  }

  async dbgetData() {
    // await this.db.dbgetResponses();  => Cannot read property 'dbgetResponses' of undefined
    console.log('dbgetData() called');
    // eslint-disable-next-line no-unused-vars
    const data = await this.db.dbgetResponses().catch(err => {
      return [];
    });
    // const data = await readFile(this.datafile, 'utf8');
    // return JSON.parse(data);
    // console.log('dbgetData ', data);
    return data;
  }
}

module.exports = ResponseService;