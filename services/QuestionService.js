// import { dbgetQuestions, query } from './db.js';

const fs = require('fs');
const util = require('util');
const db = require('./db');

const readFile = util.promisify(fs.readFile);

/**
 * Logic for reading and writing feedback data
 */
class QuestionService {
  /**
   * Constructor
   * @param {*} datafile Path to a JSOn file that contains the feedback data
   */
  constructor(datafile) {
    this.datafile = datafile;
    this.db = db;
  }

  /**
   * Get all feedback items
   */
  async getList() {
    const data = await this.getData();
    return data;
  }

  /**
   * Fetches feedback data from the JSON file provided to the constructor
   */
  async getData() {
    const data = await readFile(this.datafile, 'utf8');
    if (!data) return [];
    return JSON.parse(data);
  }

  async dbgetList() {
    const data = await this.dbgetData();
    return data;
  }

  async dbgetData() {
    // await this.db.dbgetQuestions();  => Cannot read property 'dbgetQuestions' of undefined
    console.log('dbgetData() called');
    // eslint-disable-next-line no-unused-vars
    const data = await this.db.dbgetQuestions().catch(err => {
      return [];
    });
    // const data = await readFile(this.datafile, 'utf8');
    // return JSON.parse(data);
    // console.log('dbgetData ', data);
    return data;
  }
}

module.exports = QuestionService;
