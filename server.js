const express = require('express');
const path = require('path');
const cookieSession = require('cookie-session');
const bodyParser = require('body-parser');

const SurveyService = require('./services/SurveyService');
const GenerateService = require('./services/GenerateService')
const ResponseService = require('./services/ResponseService');
const LoginService = require('./services/LoginService');
const QuestionService = require('./services/QuestionService');

const surveyService = new SurveyService('./data/surveys.json');
const generateService = new GenerateService('./data/generate.json');
const responseService = new ResponseService('./data/responses.json');
const loginService = new LoginService('');
const questionService = new QuestionService('./data/questions.json');

const routes = require('./routes');

const app = express();

const port = 1956;

app.set('trust proxy', 1);

app.use(cookieSession({
  name: 'session',
  keys: ['Gheybdudb7648723146', 'haheuahfiu23846832'],
}))

app.use(bodyParser.urlencoded({ extended: true }));

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, './views'));

app.locals.siteName = 'Moondawgs';

app.use(express.static(path.join(__dirname, './sources')));

app.use(async (request, response, next) => {
  const surveys = await surveyService.getList();
  const dbsurveys = await surveyService.dbgetData();
  const responses = await responseService.getList();
  const dbresponses = await responseService.dbgetList();
  const dbquestions = await questionService.dbgetList(); 
  
  response.locals.surveys = surveys;
  response.locals.dbsurveys = dbsurveys;
  response.locals.responses = responses;
  response.locals.dbresponses = dbresponses;
  response.locals.dbquestions = dbquestions;
  return next();
});

app.use('/', routes({
  surveyService,
  generateService,
  responseService,
  loginService,
  questionService,
}));

app.listen(port, () => {
    console.log(`Express server listening on port ${port}!`);
  });