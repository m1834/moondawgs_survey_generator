/* The questions can be thought of as tree */

const express = require('express');
var numOfQuestions = 1; // The page currently starts with 1 question

// This whole form is basically structured like a big tree
var numOfQuestions = 0;
function addQuestion() {
    var new_question = document.getElementById("questionTemplate").cloneNode(true);
    new_question.style.visibilty = "visible";
    new_question.id = "question" + (numOfQuestions);
    var childNode = new_question.children;

    for (var i = 0; i < childNode.length; i++) {
        if (childNode[i].id === "listOfAnswers") {
            childNode[i].id = ("listOfAnswers" + numOfQuestions);
        }
    }
    numOfQuestions++;
    document.getElementById("listOfQuestions").appendChild(new_question);
}

function removeQuestion(selected_question) {
    var question = selected_question.parentNode.parentNode;
    document.getElementById("listOfQuestions").removeChild(question);
}

function addAnswer(choice) {
    var value;
    var newValue;
    var input;
    var nthNode = choice.parentNode.parentNode.children;
    var qType;

    for(var i = 0; i < nthNode.length; i++) {
        if(nthNode[i].id === "answerForm") {
            for(var j = 0; j < nthNode[i].length; j++) {
                if(nthNode[i].children[j].id === "optionType") {
                    var chosenNode = nthNode[i].children[j];
                    var nodeIndex = chosenNode.selectedIndex
                    qType = chosenNode.options[nodeIndex].value;
                }
            }
        }
    }

    for (var i = 0; i < nthNode.length; i++) {
        if (nthNode[i].id.startsWith("listOfAnswers"))
            value = document.getElementById(nthNode[i].id);
    }

    // Conditions to create the appropriate question types
    if (qType === "Multiple Choice") {
        newValue = document.createElement("li");
        newValue.style = "float: inherit;" // Keeps format nice
        input = document.createElement("input");
        input.placeholder = input.id = "Enter Answer";
        newValue.appendChild(input);
    }
    else if (qType === "Likert") {
        var scale = 5;
        newValue = document.createElement("ul");
        newValue.class = "likert";

        // Create list of the likert scale for the question
        for (var i = 0; i < scale; i++) {
            var val = document.createElement("li");
            val.class = "likert";
            //textInput = document.createElement("input");
            input = document.createElement("input");
            var textArea = document.createElement("a");

            if (i == 0) {
                textArea.innerText = "Strongly Agree";
            }
            else if (i == 1) {
                textArea.innerText = "Somewhat Agree";
            }
            else if (i == 2) {
                textArea.innerText = "Neutral"
            }
            else if (i == 3) {
                textArea.innerText = "Somewhat Disagree"
            }
            else if (i == 4) {
                textArea.innerText = "Strongly Disagree";
            }
            input.type = "radio";
            input.name = "Likert";
            input.value = i + 1;
            val.appendChild(textArea);

            newValue.appendChild(val);
        }
    }
    else if (qType === "Short Answer") {
        var textField = document.createElement("textarea");
        newValue = document.createElement("li");
        newValue.style = "float: inherit;"
        textField.placeholder = "Enter Response";
        newValue.appendChild(textField);
    }
    value.appendChild(newValue);
}

function removeAnswer(choice) {
    var selected_node = choice.parentNode.parentNode.children;
    var newLine;

    for (var i = 0; i < selected_node.length; i++) {
        if (selected_node[i].id.startsWith("listOfAnswers")) {
            newLine = document.getElementById(selected_node[i].id);
        }
    }
    newLine.removeChild(newLine.lastChild);
}

function testPrint() {
    console.log("FUNCTION FOUND");
}

module.exports = {
    addQuestion, 
    removeQuestion,
    addAnswer,
    removeAnswer,
    testPrint
}
